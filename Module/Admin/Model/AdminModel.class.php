<?php
namespace Admin\Model;
use \Think\Model;
class AdminModel extends Model{
	public function _initialize(){

	}
	public function _after_select(&$result){
		foreach ($result as $k => $v) {
			$result[$k]['last_login_time'] = _date($v['last_login_time']);
		}
	}
	public function _after_find(&$result){
		$result['last_login_time'] = _date($result['last_login_time']);
	}
}
?>