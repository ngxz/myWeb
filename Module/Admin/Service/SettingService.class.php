<?php
namespace Admin\Service;
use \Think\Service;
class SettingService extends Service{
	public function __construct(){
		$this->model = D('Admin/Setting');
	}
	public function find(){
		$result = $this->model->select();
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		$data = array();
		foreach($result as $k => $v){
			$data[$v['key']] = $v['value'];
		}
		return $data;
	}
	public function update($params){
		foreach ($params as $k => $v) {
			$sqlmap = array();
			$sqlmap['key'] = $k;
			$row = $this->model->where($sqlmap)->find();
			$data = array();
			$data['key'] = $k;
			$data['value'] = $v;
			if(!$row){
				$this->model->add($data);
				continue;
			}
			$result = $this->model->where($sqlmap)->save($data);
		}
		return true;
	}
	/**
	 * 把指定文章的url提交给百度收录，加快爬虫抓取速度
	 * @param  [array] $params [文章链接的数组]
	 * @return [type]         [description]
	 */
	public function pushUrlToBaidu($params){
		if(false === strripos($params,'http')){
			$this->error = '请参考格式http://www.example.com/1.html';
			return false;
		}
		$urls = explode(',',$params);
		$api = 'http://data.zz.baidu.com/urls?site=yuanrb.com&token=I9fEHlx4pIMl25lQ';
		$ch = curl_init();
		$options =  array(
		    CURLOPT_URL => $api,
		    CURLOPT_POST => true,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POSTFIELDS => implode("\n", $urls),
		    CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
		);
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		if(!$result['success']){
			return false;
		}
		return $result;
	}
}
?>