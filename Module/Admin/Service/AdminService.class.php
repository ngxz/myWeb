<?php
namespace Admin\Service;
use \Think\Service;
class AdminService extends Service{
	public function __construct(){
		$this->model = D('Admin/Admin');
	}
	public function find($id){
		$result = $this->model->find($id);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return $result;
	}
	public function update($params){
		if(!$params['id']){
			$this->error = 'id错误';
			return false;
		}
		$data = array();
		$data['username'] = $params['username'];
		$data['name'] = $params['name'];
		// if(!empty($params['password'])){
		// 	$data['password'] = md5($params['password']);
		// }
		$result = $this->model->where(array('id'=>$params['id']))->save($data);
		if ($result === FALSE) {
			$this->error = $this->model->getError();
			return false;
		}
		return true;
	}
}
?>