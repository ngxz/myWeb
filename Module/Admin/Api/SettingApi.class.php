<?php
namespace Admin\Api;
use \Think\Api;
class SettingApi extends Api{
	public function __construct(){
		$this->settingService = D('Admin/Setting','Service');
	}
	public function site($params){
		$result = $this->settingService->find($params['comid']);
		$this->result = $result;
		return $this;
	}
}
?>