<?php
namespace Admin\Controller;
use Think\Controller;
class LoginController extends Controller {
	public function _initialize(){
        $this->service = D('Login/Login','Service');
    }
    public function _empty(){
        redirect(U('Admin/Login/login'),5,'该页面不存在，5秒后跳转调转到登录');
    }
    public function login(){
    	if (IS_POST){
            $result = $this->service->login(I('post.'));
            if(!$result){
                $this->ajaxReturn(return_data($this->service->getError()));
            }
            $this->ajaxReturn(return_data('',U('Admin/Index/index'),true));
        }
        $this->display();
    }
    public function logout(){
        session(null);
        echo "<script>";
        echo "window.top.location.href = "."'".U('Login/login')."'";
        echo "</script>";
    }
}