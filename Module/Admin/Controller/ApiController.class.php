<?php
namespace Admin\Controller;
use Api\Controller\ValidApiController;
class ApiController extends ValidApiController{
	public function _initialize(){
		parent::_initialize();
		$this->settingApi = D('Admin/Setting','Api');
	}
	public function site(){
		$result = $this->settingApi->site($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
}
?>