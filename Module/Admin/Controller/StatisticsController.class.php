<?php
namespace Admin\Controller;
use Admin\Controller\PublicController;
class StatisticsController extends PublicController {
	public function _initialize(){
		parent::_initialize();
		$this->Service = D("Statistics/Statistics","Service");
		$this->articleService = D("Statistics/Article","Service");
	}

    public function index(){
	    $userInfo = $this->Service->userInfo();//登录用户信息
	    // $addArticleMonth = $this->articleService->addArticleMonth();
	    $addArticleYear = $this->articleService->addArticleYear(2019);//指定年每月发布文章数
	    // var_dump(json_encode($addArticleYear));die();
	    $readArticleTop = $this->articleService->readArticleTop();
	    $pariseArticleTop = $this->articleService->pariseArticleTop();

	    $this->assign('userInfo',$userInfo)->assign('addArticleYear',$addArticleYear)
	    ->assign('readArticleTop',$readArticleTop)->assign('pariseArticleTop',$pariseArticleTop)
	    ->display();
	}

	/**
	 * 查看某一年的数据
	 * @return [type] [description]
	 */
	public function getAddArticleYearJson(){
		$addArticleYear = $this->articleService->addArticleYear(I('year'));
		echo json_encode($addArticleYear);
	}
}