<?php
namespace Admin\Controller;
use Admin\Controller\PublicController;
class AdminController extends PublicController {
	public function _initialize(){
        parent::_initialize();
        $this->service = D('Admin/Admin','Service');
    }
    public function _empty(){
        redirect(U('Admin/Login/login'),5,'该页面不存在，5秒后跳转调转到登录');
    }
    public function index(){
        $row = $this->service->find($_SESSION['uid']);
        $this->assign("row",$row)->display();
    }
    public function update(){
        $result = $this->service->update(I('param.'));
        if(!$result){
            $this->ajaxReturn(return_data($this->service->getError()));
        }
        $this->ajaxReturn(return_data('',U('Admin/Admin/index'),true));
    }
}