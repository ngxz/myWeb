<?php
namespace Admin\Controller;
use Admin\Controller\PublicController;
class SettingController extends PublicController {
	public function _initialize(){
		parent::_initialize();
		$this->settingService = D("setting","Service");
	}
    public function lists(){
    	$site = $this->settingService->find();
        $this->assign("site",$site)->display();
    }
    public function update(){
        $result = $this->settingService->update(I('param.'));
        if(!$result){
            $this->ajaxReturn(return_data($this->settingService->getError()));
        }
        $this->ajaxReturn(return_data('',U('Admin/Setting/lists'),true));
    }
    public function pushUrlToBaidu(){
        if(IS_POST){
            $params = I("param.");
            $result = $this->settingService->pushUrlToBaidu($params['urlList']);
            if(!$result){
                $this->ajaxReturn(return_data($this->settingService->getError()));
            }
            $this->ajaxReturn(return_data($result,U('Admin/Setting/pushUrlToBaidu'),true));
        }
        $this->display();
    }
}