<?php
namespace Admin\Controller;
use Admin\Controller\PublicController;
class CarouselController extends PublicController {
	public function _initialize(){
        parent::_initialize();
        $this->service = D('Carousel/Carousel','Service');
        $this->uploadService = D('Attachments/Uploads','Service');
    }
    public function _empty(){
        redirect(U('Admin/Login/login'),5,'该页面不存在，5秒后跳转调转到登录');
    }
    public function lists(){
        $params = I('param.');
        $count = $this->service->count($params);
    	$lists = $this->service->lists($params,$params['page'],$params['limit'],$params['order']);
        $this->assign('page',$page)->assign('count',$count)->assign('lists',$lists)->display();
    }
    public function add(){
        if(IS_POST){
            $result = $this->service->add(I('param.'));
            if(!$result){
                $this->ajaxReturn(return_data($this->service->getError()));
            }
            $this->ajaxReturn(return_data('',U('Admin/Carousel/lists'),true));
        }
        $this->display();
    }
    public function update(){
        if(IS_POST){
            $result = $this->service->update(I('param.'));
            if(!$result){
                $this->ajaxReturn(return_data($this->service->getError()));
            }
            $this->ajaxReturn(return_data('',U('Admin/Carousel/lists'),true));
        }
        $row = $this->service->find(I('param.id'));
        $this->assign('row',$row)->display();
    }
    public function delete(){
        $result = $this->service->delete(I('param.ids'));
        if(!$result){
            $this->ajaxReturn(return_data($this->service->getError()));
        }
        $this->ajaxReturn(return_data('',U('Admin/Carousel/lists'),true));
    }
    public function upload(){
        $config = array();
        $type = $_GET['type'];
        $config['savePath'] = $type ? '/Layedit/' : '/Carousel/';
        $config['comid'] = $this->comid;
        $result = $this->uploadService->set_config($config)->uploader();
        // 富文本上传图片
        if($type && $result){
            $data = array();
            $data['code'] = 0;
            $data['msg'] = '成功！';
            $data['data'] = array('src'=>get_host().'./Uploads'.$result['file']['savepath'].$result['file']['savename'],'title'=>'img');
            $this->ajaxReturn($data);
        }
        $this->ajaxReturn($result);
    }
}