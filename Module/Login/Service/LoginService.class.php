<?php
namespace Login\Service;
use \Think\Service;
class LoginService extends Service{
    public function __construct(){
        $this->model = D('admin');
    }
    /**
     * 验证帐号密码
     * @param unknown $params
     */
    public function login($params){
        if (!$params['username']){
            $this->error = '用户名不能为空';
            return false;
        }
        if (!$params['password']){
            $this->error = '密码不能为空';
            return false;
        }
        $sqlmap = array();
        $sqlmap['username'] = $params['username'];
        $result = $this->model->where($sqlmap)->find();
        if (!$result){
            $this->error = '用户名错误';
            return false;
        }
        if ($result['password'] != md5($params['password'])){
            $this->error = '密码错误';
            return false;
        }
        $this->saveLoginMsg($result);
        return true;
    }
    public function saveLoginMsg($params){
        //保存session
        $_SESSION['uid']=$params['id'];
        $_SESSION['userInfo']=$params;
        //记录登录时间，次数
        $sqlmap = array();
        $sqlmap['id'] = $params['id'];
        $data = array();
        $data['last_login_time'] = time();
        $this->model->where($sqlmap)->save($data);
        $this->model->where($sqlmap)->setInc('login_num');
    }
    public function getError(){
        return $this->error;
    }
}

?>