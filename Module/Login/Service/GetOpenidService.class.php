<?php
namespace Login\Service;
use \Think\Service;
class GetOpenidService extends Service{
    public function __construct(){
        $this->url = 'https://api.weixin.qq.com/sns/jscode2session';
        $this->http = new \Think\Http;
    }
    public function getOpenid($params){
        if (!$params['appid']){
            $this->error = 'appid不能为空';
            return false;
        }
        if (!$params['js_code']){
            $this->error = 'code不能为空';
            return false;
        }
        $data = array();
        $data['appid'] = $params['appid'];
        $data['secret'] = '1111';
        $data['js_code'] = $params['js_code'];
        $data['grant_type'] = 'authorization_code';
        $result = $this->http->getRequest($this->url,$data);
        if (!$result['openid']){
            $this->error = $result;
            return false;
        }
        return $result;
    }
    public function getError(){
        return $this->error;
    }
}

?>