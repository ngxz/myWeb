<?php
namespace Login\Controller;
use Api\Controller\ValidApiController;
class ApiController extends ValidApiController{
	public function _initialize(){
		parent::_initialize();
		$this->loginApi = D('Login/Login','Api');
	}
	public function Login(){
		$result = $this->loginApi->login($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
}
?>