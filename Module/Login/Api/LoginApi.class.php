<?php
namespace Login\Api;
use \Think\Api;
class LoginApi extends Api{
	public function __construct(){
		$this->loginService = D('Login/Login','Service');
	}
	public function login($params){
		$result = $this->loginService->login($params);
		if(!$result){
			$this->error = $this->loginService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
}
?>