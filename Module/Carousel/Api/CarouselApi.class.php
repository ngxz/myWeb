<?php
namespace Carousel\Api;
use \Think\Api;
class CarouselApi extends Api{
	public function __construct(){
		$this->carouselService = D('Carousel/Carousel','Service');
	}
	public function lists($params){
		$result = $this->carouselService->lists($params);
		if(!$result){
			$this->error = $this->carouselService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
}
?>