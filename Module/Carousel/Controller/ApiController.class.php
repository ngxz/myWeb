<?php
namespace Carousel\Controller;
use Api\Controller\ValidApiController;
class ApiController extends ValidApiController{
	public function _initialize(){
		parent::_initialize();
		$this->carouselApi = D('Carousel/Carousel','Api');
	}
	public function Lists(){
		$result = $this->carouselApi->lists($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
}
?>