<?php
namespace Carousel\Model;
use \Think\Model;
class CarouselModel extends Model{
	public function _initialize(){

	}
	public function _after_select(&$result){
		foreach ($result as $k => $v) {
			$result[$k]['time'] = _date($v['time'],'Y-m-d');
		}
	}
	public function _after_find(&$result){
		$result['time'] = _date($result['time'],'Y-m-d');
	}
}
?>