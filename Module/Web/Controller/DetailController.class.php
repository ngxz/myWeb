<?php
namespace Web\Controller;
use Web\Controller\PublicController;
class DetailController extends PublicController{
    public function _initialize(){
        parent::_initialize();
        $this->service = D('Article/Article','Service');
    }
    public function article(){
        $row = $this->service->find(I('param.id'));
        if(!$row){
            // 没有文章显示404错误页面
            $this->display('errorPage');
        }else{
            $this->assign("row",$row)->display();
        }
    }

    /**
     * 增加1个点赞数量
     */
    public function addPriseNum(){
        $result = $this->service->addPriseNum(I('param.id'));
        if(!$result){
            $this->error = $this->service->getError();
            return false;
        }
        $this->ajaxReturn($result);
    }
    public function errorPage(){
        $this->display();
    }
}

?>