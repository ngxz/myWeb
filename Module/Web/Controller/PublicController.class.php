<?php
namespace Web\Controller;
use Think\Controller;
class PublicController extends Controller{
    public function _initialize(){

        //移动设备浏览，则切换模板
        if (ismobile()) {
			//设置默认默认视图为 Mobile
            C('DEFAULT_V_LAYER','Wap');
        }
        // 顶部导航（频道列表）
        $sqlmap = array();
        $sqlmap['order'] = 'sort ASC';
        $sqlmap['status'] = 1;
        $category = D('Article/ArticleCategory','Service')->lists($sqlmap,$page,$limit,'sort ASC');
        //页脚的友情链接
        $links = D('FriendLink/FriendLink','Service')->lists();
        //页面的SEO字及页脚备案和联系邮箱等设置
        $setting = D('Admin/Setting','Service')->find();
        $this->assign('links',$links)->assign('setting',$setting)->assign('category',$category);
    }
}

?>