<?php
namespace Web\Controller;
use Web\Controller\PublicController;
class TagRelationshipController extends PublicController{
    public function _initialize(){
        parent::_initialize();
        $this->service = D('Article/ArticleTag','Service');
        $this->articleService = D('Article/Article','Service');
    }
    public function index(){
        $params = I('param.');
    	$sqlmap = array();
    	$sqlmap['status'] = 1;
        $page = $params['page'];
        // 查询标签id对应文章id
        $sqlmap['article_ids'] = $this->service->articleIdLists($params['tag_id']);
        // 查询文章列表
        $result = $this->articleService->lists($sqlmap,$page);
        $count = $this->articleService->count($sqlmap);
        $tagRow = $this->service->find($params['tag_id']);
        $tag = $tagRow['name'];
        $this->assign('page',$page)->assign('count',$count)->assign('tag',$tag)->assign('result',$result)->display();
    }
}

?>