<?php
namespace Web\Controller;
use Web\Controller\PublicController;
class CategoryController extends PublicController{
    public function _initialize(){
        parent::_initialize();
        $this->service = D('Article/ArticleCategory','Service');
    }
    public function index(){
        $row = $this->service->find(I('param.id'));
        $this->redirect($row['url'],array('category_id'=>I('param.id')));
    }
    
}

?>