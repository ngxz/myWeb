<?php
namespace Web\Controller;
use Web\Controller\PublicController;
class IndexController extends PublicController{
	public function _initialize(){
	    parent::_initialize();
        $this->service = D('Article/Article','Service');
        $this->carouselService = D('Carousel/Carousel','Service');
        $this->articleTagService = D('Article/ArticleTag','Service');
	}
    public function index(){
        // 查询首页轮播图列表
        $carousel = $this->carouselService->lists($sqlmap,$page,$limit,'sort ASC');
        //热门文章十条
        $sqlmap = array();
        $sqlmap['status'] = 1;
        $sqlmap['is_recommend'] = 1;
        $lists = $this->service->lists($sqlmap);
        // 首页右侧最近文章栏目
        $recentList = $this->service->lists(array('status'=>1,'is_recommend'=>0));
        // 首页右侧标签及对应数量栏目
        $tags = $this->articleTagService->lists(array('status'=>1));
        $this->assign('carousel',$carousel)->assign("lists",$lists)->assign("recentList",$recentList)->assign("tags",$tags)->display();
    }
}