<?php
namespace Web\Controller;
use Web\Controller\PublicController;
class AboutController extends PublicController{
    public function _initialize(){
        parent::_initialize();
        $this->service = D('Article/Article','Service');
        $this->categoryService = D('Article/ArticleCategory','Service');
    }
    public function index(){
    	$cateRow = $this->categoryService->find(4);
        $this->assign('cateRow',$cateRow)->display();
    }
}

?>