<?php
namespace Web\Controller;
use Web\Controller\PublicController;
class WebController extends PublicController{
    public function _initialize(){
        parent::_initialize();
        $this->service = D('Article/Article','Service');
        $this->categoryService = D('Article/ArticleCategory','Service');
    }
    public function index(){
    	$params = I('param.');
    	$sqlmap = array();
    	$sqlmap['category_id'] = 2;
    	$sqlmap['status'] = 1;
    	$page = $params['page'];
    	$result = $this->service->lists($sqlmap,$page);
    	$count = $this->service->count($sqlmap);
    	$cateRow = $this->categoryService->find(2);
        $this->assign('page',$page)->assign('count',$count)->assign('cateRow',$cateRow)->assign('result',$result)->display();
    }
}

?>