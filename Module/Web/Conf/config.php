<?php
return array(
	//'配置项'=>'配置值'
    'URL_ROUTER_ON'   => true,
    'URL_ROUTE_RULES'=>array(
        //文章详情页
        //detail控制器下的article方法，传入两个数字参数
        //__ROOT__/detail/article/channelid/1/id/{$new.id}简化为
        //__ROOT__/detail/1/{$new.id}
        'detail/:id\d' =>'Detail/article',
        'cate/:id\d'=>'Category/index',
        // 首页到标签列表
        'tags/:tag_id\d'=>'TagRelationship/index'
    ),
);