<?php 
namespace Attachments\Service;
use Think\Service;
class UploadsService extends Service{
	public function __construct(){
		$this->uploadClass = new \Think\Upload();
		$this->settingService = D('Admin/Setting','Service');
	}
	public function set_config($config){
		// $setting = $this->SettingService->find($config['comid']);
		// $allow_exts = explode(',',$setting);
		// // 设置附件上传大小
		// $this->UploadClass->exts = $config['exts'] ?  $config['exts']: $allow_exts;
		// 设置附件上传类型
		$this->uploadClass->rootPath = $config['rootPath'] ?  $config['rootPath']: './Uploads/'; 
		// 设置附件上传根目录
		$this->uploadClass->savePath = $config['savePath'] ?  $config['savePath']: ''; 
		// 设置附件上传（子）目录
		return $this;
	}
	public function uploader(){
		$file = $this->uploadClass->upload();
		if(!$file){
			$this->error = $this->uploadClass->getError();
			return false;
		}
		return $file;
	}
}

