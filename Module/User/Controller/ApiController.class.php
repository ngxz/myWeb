<?php
namespace FriendLink\Controller;
use Api\Controller\ValidApiController;
class ApiController extends ValidApiController{
	public function _initialize(){
		parent::_initialize();
		$this->FriendLinkApi = D('FriendLink/FriendLink','Api');
	}
	public function Lists(){
		$result = $this->FriendLinkApi->lists($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
}
?>