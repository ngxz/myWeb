<?php
namespace FriendLink\Api;
use \Think\Api;
class FriendLinkApi extends Api{
	public function __construct(){
		$this->FriendLinkService = D('FriendLink/FriendLink','Service');
	}
	public function lists($params){
		$result = $this->FriendLinkService->lists($params);
		if(!$result){
			$this->error = $this->FriendLinkService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
}
?>