<?php
namespace User\Model;
use \Think\Model;
class UserModel extends Model{
	public function _initialize(){

	}
	public function _after_select(&$result){
		foreach ($result as $k => $v) {
			$result[$k]['time'] = _date($v['time']);
			if(strpos($v['avater'],'https') === false){
                $result[$k]['avater'] = parse_attachment_url($v['avater']);
            }
		}
	}
	public function _after_find(&$result){
		$result['time'] = _date($result['time']);
		if(strpos($result['avater'], 'https') === false){
			$result['avater'] = parse_attachment_url($result['avater']);
		}
	}
}
?>