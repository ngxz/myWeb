<?php
namespace User\Service;
use Think\Service;
class UserService extends Service{
	public function __construct(){
		$this->model = D('User/User');
	}
	public function lists($sqlmap,$page,$limit,$order){
		$page = $page ? $page : 1;
		$limit = $limit ? $limit : 10;
		$order = $order ? $order : 'time DESC';
		$result = $this->model->where($this->parseSql($sqlmap))->page($page)->limit($limit)->order($order)->select();
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return $result;
	}
	public function add($params){
		if(!$this->verify($params)) return false;
		$row = $this->model->where(array('openid'=>$params['openid']))->find();
		if ($row) {
			$this->error = '会员已经存在！';
			return false;
		}
		$params['time'] = time();
		$result = $this->model->add($params);
		if(!$result){
			$this->model->getError();
			return false;
		}
		return $result;
	}
	public function count($sqlmap){
		return (int)$this->model->where($this->parseSql($sqlmap))->count();
	}
	public function update($params){
		$row = $this->model->where(array('id'=>$params['mid']))->find();
		if (!$row) {
			$this->error = '会员不存在！';
			return false;
		}
		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];
		}
		if($params['avater']){
			$data['avater'] = $params['avater'];
		}
		if(isset($params['status'])){
			$data['status'] = $params['status'];
		}
		$result = $this->model->where(array('id'=>$params['mid']))->save($data);
		if($result === false){
			$this->error = '修改失败！';
			return false;
		}
		return true;
	}
	public function find($id){
		$result = $this->model->find($id);
		if(!$result){
			$this->error = '数据不存在！';
			return false;
		}
		return $result;
	}
	public function findByOpenid($openid){
		$result = $this->model->where(array('openid'=>$openid))->find();
		if(!$result){
			return false;
		}
		return $result;
	}
	public function delete($ids){
		if(stripos($ids,',')){
			$ids = explode(',',$ids);
		}else{
			$ids = $ids;
		}
		if(!count($ids)){
			$this->error = 'id不存在！';
			return false;
		}
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$ids);
		$result = $this->model->where($sqlmap)->delete();
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return true;
	}
	public function getOpenid($params){
		if(!$params['code']){
			$this->error = 'code不存在!';
			return false;
		}
		if(!$params['appid']){
			$this->error = 'appid不存在!';
			return false;
		}
		$http = new \Think\Http();
		$url = 'https://xcode.7ying.cc/index.php/shopapi/getopenid/';
		$data = array();
		$data['code'] = $params['code'];
		$data['appid'] = $params['appid'];
		$result = json_decode($http->getRequest($url,$data),true);
		if($result['status']){
			$this->error = $result['text'];
			return false;
		}
		return $result['openid'];
	}
	public function parseSql($sqlmap){
		$_sqlmap = array();
		if($sqlmap['name']){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['name'].'%');
		}
		if($sqlmap['status']){
			$_sqlmap['status'] = $sqlmap['status'];
		}
		return $_sqlmap;
	}
	private function verify($params){
		if(!$params['openid']){
			$this->error = 'openid错误';
			return false;
		}
		return true;
	}
}

?>