<?php
namespace FriendLink\Controller;
use Api\Controller\ValidApiController;
class ApiController extends ValidApiController{
	public function _initialize(){
		parent::_initialize();
		$this->friendLinkApi = D('FriendLink/FriendLink','Api');
	}
	public function Lists(){
		$result = $this->friendLinkApi->lists($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
}
?>