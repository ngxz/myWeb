<?php
namespace FriendLink\Api;
use \Think\Api;
class FriendLinkApi extends Api{
	public function __construct(){
		$this->friendLinkService = D('FriendLink/FriendLink','Service');
	}
	public function lists($params){
		$result = $this->friendLinkService->lists($params);
		if(!$result){
			$this->error = $this->friendLinkService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
}
?>