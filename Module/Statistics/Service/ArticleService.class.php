<?php
namespace Statistics\Service;
use \Think\Service;
class ArticleService extends Service{
    public function __construct(){
        $this->model = D('Article/Article');
    }
    
    /**
     * 新增文章的月数据
     */
    public function addArticleMonth($date = ''){
        $sqlmap = array();
        if($date != ''){
            $month_start = strtotime($date);//指定月份月初时间戳
            $month_end = mktime(23, 59, 59, date('m', strtotime($date))+1, 00);//指定月份月末时间戳
        }else{
            $month_start = mktime(0, 0 , 0,date("m"),1,date("Y"));
            $month_end = mktime(23,59,59,date("m"),date("t"),date("Y"));
        }
        $sqlmap['time'] = array(array('EGT',$month_start),array('ELT',$month_end));
        $result = $this->model->where($sqlmap)->count();
        return (int)$result;
    }

    /**
     * 发布文章的年数据
     * @param string $date [年份，默认为今年]
     */
    public function addArticleYear($date = ''){
        $year = _date(time(),'Y');
        $result = array();
        if($date != ''){
            $year = $date;
        }
        for ($i=1; $i <=12 ; $i++) {
            $_date = $year.'-'.$i;
            $month_start = strtotime($_date);//指定月份月初时间戳
            $month_end = mktime(23, 59, 59, date('m', strtotime($_date))+1, 00);//指定月份月末时间戳
            $sqlmap = array();
            $sqlmap['time'] = array(array('EGT',$month_start),array('ELT',$month_end));
            // $result[$i] = $this->model->where($sqlmap)->count();
            $result[] = $this->model->where($sqlmap)->count();
        }
        return $result;
    }

    /**
     * 文章阅读数最多的
     * @param  integer $limit [默认5条]
     * @param  string  $order [description]
     * @return [type]         [description]
     */
    public function readArticleTop($limit = 5,$order = 'read_num DESC'){
        $result = $this->model->field(array('id','name','read_num'))->limit($limit)->order($order)->select();
        return $result;
    }

    /**
     * 文章点赞数最多的
     * @param  integer $limit [默认5条]
     * @param  string  $order [description]
     * @return [type]         [description]
     */
    public function pariseArticleTop($limit = 5,$order = 'parise_num DESC'){
        $result = $this->model->field(array('id','name','parise_num'))->limit($limit)->order($order)->select();
        return $result;
    }

    public function getError(){
        return $this->error;
    }
}

?>