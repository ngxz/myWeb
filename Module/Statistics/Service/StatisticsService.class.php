<?php
namespace Statistics\Service;
use \Think\Service;
class StatisticsService extends Service{
    public function __construct(){
        $this->adminService = D('Admin/Admin','Service');
    }
    
    /**
     * 当前登录管理员的信息
     * @return [type] [description]
     */
    public function userInfo(){
        $result = $this->adminService->find($_SESSION['uid']);
        return $result;
    }

    public function getError(){
        return $this->error;
    }
}

?>