<?php
namespace Article\Model;
use \Think\Model;
class ArticleModel extends Model{
	public function _initialize(){

	}
	public function _after_select(&$result){
		foreach ($result as $k => $v) {
			$result[$k]['time'] = _date($v['time'],'Y-m-d');
			$result[$k]['content'] = htmlspecialchars_decode($v['content']);
		}
	}
	public function _after_find(&$result){
		$result['time'] = _date($result['time'],'Y-m-d');
		$result['content'] = htmlspecialchars_decode($result['content']);
	}
}
?>