<?php
namespace Article\Api;
use \Think\Api;
class ArticleTagApi extends Api{
	public function __construct(){
		$this->articleTagService = D('Article/ArticleTag','Service');
	}
	public function lists($params){
		$result = $this->articleTagService->lists($params,$params['page'],$params['limit'],$params['order']);
		if(!$result){
			$this->error = $this->articleTagService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
	public function detail($params){
		if(!$params['id']){
			$this->error = 'id错误！';
			return $this;
		}
		$result = $this->articleTagService->find($params['id']);
		if(!$result){
			$this->error = $this->articleTagService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
}
?>