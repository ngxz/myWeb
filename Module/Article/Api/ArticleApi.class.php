<?php
namespace Article\Api;
use \Think\Api;
class ArticleApi extends Api{
	public function __construct(){
		$this->articleService = D('Article/Article','Service');
	}
	public function lists($params){
		$result = $this->articleService->lists($params,$params['page'],$params['limit'],$params['order']);
		if(!$result){
			$this->error = $this->articleService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
	public function detail($params){
		if(!$params['id']){
			$this->error = 'id错误！';
			return $this;
		}
		$result = $this->articleService->find($params['id']);
		if(!$result){
			$this->error = $this->articleService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
}
?>