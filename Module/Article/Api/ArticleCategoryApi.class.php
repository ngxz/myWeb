<?php
namespace Article\Api;
use \Think\Api;
class ArticleCategoryApi extends Api{
	public function __construct(){
		$this->articleCategoryService = D('Article/ArticleCategory','Service');
	}
	public function lists($params){
		$result = $this->articleCategoryService->lists($params,$params['page'],$params['limit'],$params['order']);
		if(!$result){
			$this->error = $this->articleCategoryService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
	public function detail($params){
		if(!$params['id']){
			$this->error = 'id错误！';
			return $this;
		}
		$result = $this->articleCategoryService->find($params['id']);
		if(!$result){
			$this->error = $this->articleCategoryService->getError();
			return $this;
		}
		$this->result = $result;
		return $this;
	}
}
?>