<?php
namespace Article\Service;
use \Think\Service;
class ArticleService extends Service{
	public function __construct(){
		$this->model = D('Article/Article');
		$this->articleCategoryService = D('Article/ArticleCategory','Service');
		$this->articleTagService = D('Article/ArticleTag','Service');
		$this->tagRelationshipModel = D('tag_relationship');
	}
	public function lists($sqlmap,$page,$limit,$order){
		$page = $page ? $page : 1;
		$limit = $limit ? $limit : 10;
		$order = $order ? $order : 'time DESC';
		$result = $this->model->where($this->parseSql($sqlmap))->page($page)->limit($limit)->order($order)->select();
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		// 类别
		foreach ($result as $k => $v) {
			$res = $this->articleCategoryService->find($v['category_id']);
			$result[$k]['category'] = $res['name'];
		}
		return $result;
	}
	public function find($id){
		$result = $this->model->find($id);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		// 累加阅读量(后台访问不应该算入)
		$this->model->where(array('id'=>$id))->setInc('read_num');
		$result['category'] = $this->articleCategoryService->find($result['category_id']);
		// 根据文章id查找对应标签id
		$tagRelationship = $this->tagRelationshipModel->field('tag_id')->where(array('article_id'=>$id))->select();
		$result['tagRelationship'] = array();
		foreach ($tagRelationship as $k => $v) {
			$result['tagRelationship'][$k] = $v['tag_id'];
		}
		// 根据文章的类别查找该类别文章列表
		$_sqlmap = array();
		$_sqlmap['category_id'] = $result['category_id'];
		$rows = $this->lists($_sqlmap,1,5);
		$relativeArticle = array();
		foreach ($rows as $k => $v) {
			// 过滤自己
			if($v['id'] != $id){
				$relativeArticle[$k]['id'] = $v['id'];
				$relativeArticle[$k]['name'] = $v['name'];
			}
		}
		$result['relativeArticle'] = $relativeArticle;
		return $result;
	}
	public function add($params){
		if(!$this->verify($params)) return false;
		$params['sort'] = $params['sort'] ? $params['sort'] : 99;
		$params['time'] = time();
		if(!$params['summary']){
			// 截取内容作为摘要
			$params['summary'] = $this->setSummary($params['content']);
		}
		if(!$params['cover']){
			// 设置默认封面,随笔，前端，php
			$params['cover'] = $this->setCover($params['category_id']);
		}
		$result = $this->model->add($params);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		// 文章标签关联表插入数据，如果有的话
		if(!empty($params['tag_ids'])){
			$data = array();
			$data['article_id'] = $result;
			$data['time'] = time();
			$tag_ids = $params['tag_ids'];
			if(strpos($tag_ids,',')){
				$tags = explode(',',$tag_ids);
				foreach ($tags as $k => $v) {
					$data['tag_id'] = $v;
					$this->tagRelationshipModel->add($data);
				}
			}else{
				$data['tag_id'] = $tag_ids;
				$this->tagRelationshipModel->add($data);
			}
		}
		return true;
	}
	public function delete($ids){
		if(!$ids){
			$this->error = 'id错误';
			return false;
		}
		if(strpos($ids,',')){
			$ids = explode(',', $ids);
		}else{
			$ids = $ids;
		}
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$ids);
		$result = $this->model->where($sqlmap)->delete();
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		// 删除标签关联表和文章关联的
		$this->tagRelationshipModel->where(array('article_id'=>array('IN',$ids)))->delete();
		return true;
	}
	public function update($params){
		if(!$this->verify($params)) return false;
		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		if($params['author']){
			$data['author'] = $params['author'];
		}
		if($params['summary']){
			$data['summary'] = $params['summary'];	
		}
		// 文章标签关联表插入数据，如果有的话
		if(!empty($params['tag_ids'])){
			$_data = array();
			$_data['article_id'] = $params['id'];
			$_data['time'] = time();
			$tag_ids = $params['tag_ids'];
			// 删除该文章相关的标签关联
			$this->tagRelationshipModel->where(array('article_id'=>$params['id']))->delete();
			// 添加新的关联
			if(strpos($tag_ids,',')){
				$tags = explode(',',$tag_ids);
				foreach ($tags as $k => $v) {
					$_data['tag_id'] = $v;
					$this->tagRelationshipModel->add($_data);
				}
			}else{
				$_data['tag_id'] = $tag_ids;
				$this->tagRelationshipModel->add($_data);
			}
		}
		if($params['content']){
			$data['content'] = $params['content'];
		}
		if($params['cover']){
			$data['cover'] = $params['cover'];
		}
		if($params['category_id']){
			$data['category_id'] = $params['category_id'];
		}
		if(isset($params['is_recommend'])){
			$data['is_recommend'] = $params['is_recommend'];
		}
		if($params['parise_num']){
			$data['parise_num'] = $params['parise_num'];
		}
		if($params['read_num']){
			$data['read_num'] = $params['read_num'];
		}
		if($params['sort']){
			$data['sort'] = $params['sort'];
		}
		if(isset($params['status'])){
			$data['status'] = $params['status'];
		}
		$result = $this->model->where(array('id'=>$params['id']))->save($data);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return true;
	}
	public function count($sqlmap){
		return (int)$this->model->where($this->parseSql($sqlmap))->count();
	}
	private function parseSql($sqlmap){
		$_sqlmap = array();
		if($sqlmap['name']){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['name'].'%');
		}
		if($sqlmap['category_id']){
			$_sqlmap['category_id'] = $sqlmap['category_id'];
		}
		if(isset($sqlmap['is_recommend'])){
			$_sqlmap['is_recommend'] = $sqlmap['is_recommend'];
		}
		if(isset($sqlmap['status'])){
			$_sqlmap['status'] = $sqlmap['status'];
		}
		if(isset($sqlmap['tags'])){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['tags'].'%');
		}
		if(isset($sqlmap['article_ids'])){
			$_sqlmap['id'] = array('IN',$sqlmap['article_ids']);
			if(empty($sqlmap['article_ids'])){
				$_sqlmap['id'] = -1;
			}
		}
		return $_sqlmap;
	}
	private function verify($params){
		if(!$params['name']){
			$this->error = '文章名称错误';
			return false;
		}
		if(!$params['content']){
			$this->error = '文章内容错误！';
			return false;
		}
		if(!$params['category_id']){
			$this->error = '文章分类错误！';
			return false;
		}
		return true;
	}
	private function setSummary($params){
		if(strlen($params) > 50){
			$_str = mb_substr($params,0,100,"utf-8");
			return $_str;
		}
		return $params;
	}
	private function setCover($category_id){
		switch ($category_id) {
			case 1:
				// 随笔
				return '/Public/static/img/thumb.jpg';
				break;
			case 2:
				// 前端
				return '/Public/static/img/web_default.jpg';
				break;
			case 3:
				// php
				return '/Public/static/img/php_default.jpg';
				break;
		}
	}


	public function addPriseNum($id){
		if(empty($id)){
			$this->error = 'id错误';
			return false;
		}
		$result = $this->model->where(array('id'=>$id))->setInc('parise_num',1);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		$row = $this->model->field('parise_num')->where(array('id'=>$id))->find();
		return $row['parise_num'];
	}
}
?>