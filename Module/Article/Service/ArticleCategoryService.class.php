<?php
namespace Article\Service;
use \Think\Service;
class ArticleCategoryService extends Service{
	public function __construct(){
		$this->model = D('Article/ArticleCategory');
	}
	public function lists($sqlmap,$page,$limit,$order){
		$page = $page ? $page : 1;
		$limit = $limit ? $limit : 10;
		$order = $order ? $order : 'time DESC';
		$result = $this->model->where($this->parseSql($sqlmap))->page($page)->limit($limit)->order($order)->select();
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return $result;
	}
	public function find($id){
		$result = $this->model->find($id);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return $result;
	}
	public function add($params){
		if(!$this->verify($params)) return false;
		$params['sort'] = $params['sort'] ? $params['sort'] : 99;
		$params['status'] = $params['status'] ? $params['status'] : 1;
		$params['time'] = time();
		$result = $this->model->add($params);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return true;
	}
	public function delete($ids){
		if(!$ids){
			$this->error = 'id错误';
			return false;
		}
		if(strpos($ids,',')){
			$ids = explode(',', $ids);
		}else{
			$ids = $ids;
		}
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$ids);
		$result = $this->model->where($sqlmap)->delete();
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return true;
	}
	public function update($params){
		if(!$this->verify($params)) return false;
		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		if($params['url']){
			$data['url'] = $params['url'];	
		}
		if($params['introduce']){
			$data['introduce'] = $params['introduce'];	
		}
		if($params['sort']){
			$data['sort'] = $params['sort'];
		}
		if(isset($params['status'])){
			$data['status'] = $params['status'];
		}
		$result = $this->model->where(array('id'=>$params['id']))->save($data);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		return true;
	}
	public function count($sqlmap){
		return (int)$this->model->where($this->parseSql($sqlmap))->count();
	}
	private function parseSql($sqlmap){
		$_sqlmap = array();
		if($sqlmap['name']){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['name'].'%');
		}
		if($sqlmap['status']){
			$_sqlmap['status'] = $sqlmap['status'];
		}
		return $_sqlmap;
	}
	private function verify($params){
		if(!$params['name']){
			$this->error = '类别名称错误';
			return false;
		}
		return true;
	}
}
?>