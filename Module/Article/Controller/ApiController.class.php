<?php
namespace Article\Controller;
use Api\Controller\ValidApiController;
class ApiController extends ValidApiController{
	public function _initialize(){
		parent::_initialize();
		$this->articleApi = D('Article/Article','Api');
		$this->articleCategoryApi = D('Article/ArticleCategory','Api');
		$this->articleTagApi = D('Article/ArticleTag','Api');
	}
	public function CategoryLists(){
		$result = $this->articleCategoryApi->lists($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
	public function CategoryDetail(){
		$result = $this->articleCategoryApi->detail($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
	public function Lists(){
		$result = $this->articleApi->lists($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
	public function Detail(){
		$result = $this->articleApi->detail($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
	public function TagLists(){
		$result = $this->articleTagApi->lists($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
	public function TagDetail(){
		$result = $this->articleTagApi->detail($this->params)->output();
		$this->ajaxReturn($result,$this->type);
	}
}
?>