<?php
//时间戳转换
function _date($time,$format='Y-m-d H:i:s'){
	if(!$time) return false;
	return date($format,$time);
}
//获取网站应用实际的根host
function get_host(){
	$http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';     
	return $http_type.$_SERVER['HTTP_HOST'].__ROOT__;
}
/**
 * 字符串加解密
 * @param  string  $string   原始字符串
 * @param  string  $operation 加解密类型
 * @param  string  $key      密钥
 * @param  integer $expiry   有效期

 * @return string
 */
function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
    $ckey_length = 4;
    $key = md5($key != '' ? $key : C('authkey'));
    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';
    $cryptkey = $keya.md5($keya.$keyc);
    $key_length = strlen($cryptkey);
    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
    $string_length = strlen($string);

    $result = '';
    $box = range(0, 255);
    $rndkey = array();
    for($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }

    for($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }

    for($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }

    if($operation == 'DECODE') {
        if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        return $keyc.str_replace('=', '', base64_encode($result));
    }
}
function return_data($msg,$url,$status){
    $data = array();
    $data['msg'] = $msg ? $msg : '操作成功';
    $data['url'] = $url ? $url : '';
    $data['status']  = $status ? $status : false;
    return $data;
}
//分析附件的url
function parse_attachment_url($attachments){
    $data = array();
    if($attachments){
        if(strpos($attachments,',')){
            $attachemnts_array = array_filter(explode(',',$attachments));
            foreach($attachemnts_array as $k => $v){
                $data[$k] = get_host().$v;
            }
        }else{
            $data[] = get_host().$attachments;
        }
    }
    return $data;
}
//判断是否手机
function ismobile() {
// 如果有HTTP_X_WAP_PROFILE则一定是移动设备
if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
    return true;

    //此条摘自TPM智能切换模板引擎，适合TPM开发
    if(isset ($_SERVER['HTTP_CLIENT']) &&'PhoneClient'==$_SERVER['HTTP_CLIENT'])
        return true;
        //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset ($_SERVER['HTTP_VIA']))
            //找不到为flase,否则为true
            return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
            //判断手机发送的客户端标志,兼容性有待提高
            if (isset ($_SERVER['HTTP_USER_AGENT'])) {
                $clientkeywords = array(
                    'nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile'
                );
                //从HTTP_USER_AGENT中查找手机浏览器的关键字
                if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                    return true;
                }
            }
            //协议法，因为有可能不准确，放到最后判断
            if (isset ($_SERVER['HTTP_ACCEPT'])) {
                // 如果只支持wml并且不支持html那一定是移动设备
                // 如果支持wml和html但是wml在html之前则是移动设备
                if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                    return true;
                }
            }
            return false;
}

/**
 * 获取客户端浏览器以及版本号
 * @param $agent    //$_SERVER['HTTP_USER_AGENT']
 * @return array[browser]       浏览器名称
 * @return array[browser_ver]   浏览器版本号
 */    
function getClientBrowser($agent = '') {
    $browser = '';
    $browser_ver = '';
    if (preg_match('/OmniWeb\/(v*)([^\s|;]+)/i', $agent, $regs)) {
        $browser = 'OmniWeb';
        $browser_ver = $regs[2];
    }
    if (preg_match('/Netscape([\d]*)\/([^\s]+)/i', $agent, $regs)) {
        $browser = 'Netscape';
        $browser_ver = $regs[2];
    }
    if (preg_match('/safari\/([^\s]+)/i', $agent, $regs)) {
        $browser = 'Safari';
        $browser_ver = $regs[1];
    }
    if (preg_match('/MSIE\s([^\s|;]+)/i', $agent, $regs)) {
        $browser = 'Internet Explorer';
        $browser_ver = $regs[1];
    }
    if (preg_match('/Opera[\s|\/]([^\s]+)/i', $agent, $regs)) {
        $browser = 'Opera';
        $browser_ver = $regs[1];
    }
    if (preg_match('/NetCaptor\s([^\s|;]+)/i', $agent, $regs)) {
        $browser = '(Internet Explorer '.$browser_ver.') NetCaptor';
        $browser_ver = $regs[1];
    }
    if (preg_match('/Maxthon/i', $agent, $regs)) {
        $browser = '(Internet Explorer '.$browser_ver.') Maxthon';
        $browser_ver = '';
    }
    if (preg_match('/360SE/i', $agent, $regs)) {
        $browser = '(Internet Explorer '.$browser_ver.') 360SE';
        $browser_ver = '';
    }
    if (preg_match('/SE 2.x/i', $agent, $regs)) {
        $browser = '(Internet Explorer '.$browser_ver.') 搜狗';
        $browser_ver = '';
    }
    if (preg_match('/FireFox\/([^\s]+)/i', $agent, $regs)) {
        $browser = 'FireFox';
        $browser_ver = $regs[1];
    }
    if (preg_match('/Lynx\/([^\s]+)/i', $agent, $regs)) {
        $browser = 'Lynx';
        $browser_ver = $regs[1];
    }
    if (preg_match('/Chrome\/([^\s]+)/i', $agent, $regs)) {
        $browser = 'Chrome';
        $browser_ver = $regs[1];
    }
    if (preg_match('/MicroMessenger\/([^\s]+)/i', $agent, $regs)) {
        $browser = '微信浏览器';
        $browser_ver = $regs[1];
    }
    if ($browser != '') {
        return ['browser'=>$browser, 'browser_ver'=>$browser_ver];
    } else {
        return ['browser'=>'未知','browser_ver'=> ''];
    }
}
/**
 * 获取客户端操作系统
 * @param $agent    //$_SERVER['HTTP_USER_AGENT']
 * @return array[os]            操作系统名称
 * @return array[os_ver]        操作系统版本号
 * @return array[equipment]     终端设备类型
 */
function getClientOS($agent = ''){ 
        //window系统
        if (stripos($agent, 'window')) {
            $os = 'Windows';            
            $equipment = '电脑';            
            if (preg_match('/nt 6.0/i', $agent)) {
                $os_ver = 'Vista';
            }elseif(preg_match('/nt 10.0/i', $agent)) {
                $os_ver = '10';
            }elseif(preg_match('/nt 6.3/i', $agent)) {
                $os_ver = '8.1';
            }elseif(preg_match('/nt 6.2/i', $agent)) {
                $os_ver = '8.0';
            }elseif(preg_match('/nt 6.1/i', $agent)) {
                $os_ver = '7';
            }elseif(preg_match('/nt 5.1/i', $agent)) {
                $os_ver = 'XP';
            }elseif(preg_match('/nt 5/i', $agent)) {
                $os_ver = '2000';
            }elseif(preg_match('/nt 98/i', $agent)) {
                $os_ver = '98';
            }elseif(preg_match('/nt/i', $agent)) {
                $os_ver = 'nt';
            }else{
                $os_ver = '';
            }
            if (preg_match('/x64/i', $agent)) {
                $os .= '(x64)';
            }elseif(preg_match('/x32/i', $agent)){
                $os .= '(x32)';
            }
        }
        elseif(stripos($agent, 'linux')) {
            if (stripos($agent, 'android')) {
                preg_match('/android\s([\d\.]+)/i', $agent, $match);
                $os = 'Android';      
                $equipment = 'Mobile phone';      
                $os_ver = $match[1];
            }else{
                $os = 'Linux';
            }            
        }
        elseif(stripos($agent, 'unix')) {
            $os = 'Unix';
        }
        elseif(preg_match('/iPhone|iPad|iPod/i',$agent)) {
            preg_match('/OS\s([0-9_\.]+)/i', $agent, $match);
            $os = 'IOS';
            $os_ver = str_replace('_','.',$match[1]);
            if(preg_match('/iPhone/i',$agent)){
                $equipment = 'iPhone';  
            }elseif(preg_match('/iPad/i',$agent)){
                $equipment = 'iPad'; 
            }elseif(preg_match('/iPod/i',$agent)){
                $equipment = 'iPod'; 
            }
        }
        elseif(stripos($agent, 'mac os')) {
            preg_match('/Mac OS X\s([0-9_\.]+)/i', $agent, $match);
            $os = 'Mac OS X';
            $equipment = '电脑'; 
            $os_ver = str_replace('_','.',$match[1]);
        }
        else {
            $os = 'Other';
        }
        return ['os'=>$os, 'os_ver'=>$os_ver, 'equipment'=>$equipment];
}

/**
 * 获取客户端手机型号
 * @param $agent    //$_SERVER['HTTP_USER_AGENT']
 * @return array[mobile_brand]      手机品牌
 * @return array[mobile_ver]        手机型号
 */
function getClientMobileBrand($agent = ''){
    if(preg_match('/iPhone\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = '苹果';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/SAMSUNG|Galaxy|GT-|SCH-|SM-\s([^\s|;]+)/i', $agent, $regs)) {
         $mobile_brand = '三星';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/Huawei|Honor|H60-|H30-\s([^\s|;]+)/i', $agent, $regs)) {
         $mobile_brand = '华为';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/Mi |mi \s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '小米';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/HM NOTE|HM201\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '红米';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/Coolpad|8190Q|5910\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '酷派';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/ZTE|X9180|N9180|U9180\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '中兴';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/OPPO|X9007|X907|X909|R831S|R827T|R821T|R811|R2017\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = 'OPPO';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/HTC|Desire\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = 'HTC';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/Nubia|NX50|NX40\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '努比亚';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/M045|M032|M355\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '魅族';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/Gionee|GN\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '金立';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/HS-U|HS-E\s([^\s|;]+)/i', $agent, $regs)) {        
        $mobile_brand = '海信';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/Lenove\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = '联想';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/ONEPLUS\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = '一加';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/vivo\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = 'vivo';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/K-Touch\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = '天语';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/DOOV\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = '朵唯';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/GFIVE\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = '基伍';
        $mobile_ver = $regs[0];
    }elseif(preg_match('/Nokia\s([^\s|;]+)/i', $agent, $regs)) {
        $mobile_brand = '诺基亚';
        $mobile_ver = $regs[0];
    }else{
        $mobile_brand = '其他';
        $mobile_ver = '其他';
    }
    return ['mobile_brand'=>$mobile_brand, 'mobile_ver'=>$mobile_ver];
}
function get_ip(){
    //判断服务器是否允许$_SERVER
    if(isset($_SERVER)){    
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $realip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        }else{
            $realip = $_SERVER['REMOTE_ADDR'];
        }
    }else{
        //不允许就使用getenv获取  
        if(getenv("HTTP_X_FORWARDED_FOR")){
              $realip = getenv( "HTTP_X_FORWARDED_FOR");
        }elseif(getenv("HTTP_CLIENT_IP")) {
              $realip = getenv("HTTP_CLIENT_IP");
        }else{
              $realip = getenv("REMOTE_ADDR");
        }
    }

    return $realip;
}
?>