//百度统计代码
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?e47e4f1811685155b314133ec7606da3";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();




// footer固定在底部
$(function(){
  function footerPosition(){
      $(".footer").removeClass("fixed-bottom");
      var contentHeight = document.body.scrollHeight,//网页正文全文高度
          winHeight = window.innerHeight;//可视窗口高度，不包括浏览器顶部工具栏
      if(!(contentHeight > winHeight)){
          //当网页正文高度小于可视窗口高度时，为footer添加类fixed-bottom
          $(".footer").addClass("fixed-bottom");
      } else {
          $(".footer").removeClass("fixed-bottom");
      }
  }
  footerPosition();
  $(window).resize(footerPosition);
});

