<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace Think;
class Api {

	public $code = 1;

	public $msg = '操作成功';

	public $status = 'success';

    public function getError() {
       return $this->error;
    }
    public function output(){
    	if(!$this->result){
    		$this->code = 0;
    		$this->msg = $this->error;
    		$this->status = 'fail';
    		$this->result = '';
    	}
    	$data = array();
    	$data['code'] = $this->code;
    	$data['msg'] = $this->msg;
    	$data['status'] = $this->status;
    	$data['result'] = $this->result;
    	return $data;
    }
}


