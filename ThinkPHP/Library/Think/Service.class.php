<?php
namespace Think;
class Service {

    public function getError() {
        return $this->error;
    }
    public function page($count,$listRows){
		$page = new \Think\Page($count,$listRows);
		return $page->show();
	}
	public function totalpage($limit,$count){
		$limit = $limit ? $limit : 10;
		return ceil($count/$limit);
	}
}
